<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AAM_Incorporadora
 */

?>

<div class="aam-modal">
	<div id="whatsapp" class="modal fade" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
					<div class="row">
						<div class="col-lg-6 col-xs-12">
							<img src="<?php echo home_url() ?>/wordpress/wp-content/uploads/2017/04/whatsapp-cinza.png" class="icone" alt="">
							<h2 class="modal-title">
								WhatsApp para contato
							</h2>
			        <p>Fale agora mesmo com um dos nossos consultores.</p>
							<img src="<?php echo home_url() ?>/wordpress/wp-content/uploads/2017/04/logo-aam-incorporadora-fade.png" class="logo" alt="">
						</div>
						<div class="col-lg-6 col-xs-12 relative text-center" style="min-height: 404px;">
							<a href="tel:+551198888-7777" class="valign" style="color: #000;font-size: 36px;left: 75px;font-weight: bold;">11 98888-7777</a>
						</div>
					</div>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!-- WhatsApp -->

	<div id="telefone" class="modal fade" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
					<div class="row">
						<div class="col-lg-6 col-xs-12">
							<img src="<?php echo home_url() ?>/wordpress/wp-content/uploads/2017/04/telefone-cinza.png" class="icone" alt="">
							<h2 class="modal-title">
								Telefone para contato
							</h2>
			        <p>Fale agora mesmo com um dos nossos consultores.</p>
							<img src="<?php echo home_url() ?>/wordpress/wp-content/uploads/2017/04/logo-aam-incorporadora-fade.png" class="logo" alt="">
						</div>
						<div class="col-lg-6 col-xs-12 relative text-center" style="min-height: 404px;">
							<a href="tel:+551150548300" class="valign" style="color: #000;font-size: 36px;left: 75px;font-weight: bold;">11 5054-8300</a>
						</div>
					</div>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!-- Telefone -->

	<div id="ligamos" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-6 col-xs-12">
							<img src="<?php echo home_url() ?>/wordpress/wp-content/uploads/2017/04/ligamos-para-voce-cinza.png" class="icone" alt="">
							<h2 class="modal-title">
								Ligamos para você
							</h2>
							<p>
								Nos informe o melhor dia e horário que um dos
								<br />nossos consultores entrará em contato.
							</p>
							<img src="<?php echo home_url() ?>/wordpress/wp-content/uploads/2017/04/logo-aam-incorporadora-fade.png" class="logo" alt="">
						</div>
						<div class="col-lg-6 col-xs-12 relative text-center" style="min-height: 404px;">
							<?php echo do_shortcode('[contact-form-7 id="771" title="Ligamos para você - Modal"]') ?>
						</div>
					</div>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!-- Ligamos -->

	<div id="email" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-6 col-xs-12">
							<img src="<?php echo home_url() ?>/wordpress/wp-content/uploads/2017/04/envelope-cinza-1.png" class="icone" alt="">
							<h2 class="modal-title">
								Contato
								<br />por e-mail
							</h2>
							<p>
								Para mais informações, deixe sua mensagem
								<br />que em breve um dos nossos consultores
								<br />entrarão em contato.
							</p>
							<img src="<?php echo home_url() ?>/wordpress/wp-content/uploads/2017/04/logo-aam-incorporadora-fade.png" class="logo" alt="">
						</div>
						<div class="col-lg-6 col-xs-12 relative text-center" style="min-height: 404px;">
							<?php echo do_shortcode('[contact-form-7 id="772" title="Contato por E-mail - Modal"]') ?>
						</div>
					</div>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!-- Ligamos -->

</div>

	<footer class="aam-footer">
		<div class="container">
			<div class="aam-widget aam-widget-1">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/logo.png" alt="">
				<address>
					Av. Ibirapuera, 2332, 120 andar, Conjunto 122 - Torre II - São Paulo - SP
				</address>
			</div>
			<div class="aam-widget">
				<h3 class="widget-title">Corporativo</h3>
				<ul class="list">
					<li><a href="">Quem Somos</a></li>
					<li><a href="">Contato</a></li>
					<li><a href="">Venda Seu Terreno</a></li>
					<li><a href="">Trabalhe Conosco</a></li>
					<li><a href="">Área Restrita AAM</a></li>
				</ul>
			</div>
			<div class="aam-widget">
				<h3 class="widget-title">Imóveis</h3>
				<ul class="list">
					<li><a href="">Lançamentos</a></li>
					<li><a href="">Breves Lançamentos</a></li>
					<li><a href="">Em Obra</a></li>
					<li><a href="">Prontos</a></li>
					<li><a href="">Locações</a></li>
				</ul>
			</div>
			<div class="aam-widget">
				<h3 class="widget-title">Vendas</h3>
				<ul class="list">
					<li><a href="">Atendimento Online</a></li>
					<li><a href="">Atendimento Por E-mail</a></li>
					<li><a href="">Ligamos para Você</a></li>
					<li><a href="">Telefone</a></li>
					<li><a href="">WhatsApp</a></li>
				</ul>
			</div>

			<div class="aam-widget">
				<h3 class="widget-title">Clientes e Parceiros</h3>
				<a href="" class="partner-item">
					<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/corretor.png" alt="">
					<span class="">Portal do Corretor</span>
				</a>
				<a href="" class="partner-item">
					<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/clientes.png" alt="">
					<span class="">Portal do Cliente</span>
				</a>
				<a href="" class="partner-item">
					<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/real.png" alt="">
					<span class="">Índices Financeiros</span>
				</a>
			</div>
		</div>

		<section class="aam-call">
			<div class="container no-padding">
				<span class="broker">
					Fale agora mesmo com um
					<br />dos nossos consultores:
				</span>
				<ul>
					<li>
						<a href="" class="aam-call-item" data-target="#whatsapp" data-toggle="modal">
							<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/whatsapp.png" />
							<span class="">WhatsApp</span>
						</a>
					</li>
					<li>
						<a href="" class="aam-call-item" data-target="#telefone" data-toggle="modal">
							<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/telefone.png" />
							<span class="">Telefone</span>
						</a>
					</li>
					<li>
						<a href="" class="aam-call-item" data-target="#ligamos" data-toggle="modal">
							<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/ligamos.png" />
							<span class="">Ligamos para você</span>
						</a>
					</li>
					<li>
						<a href="" class="aam-call-item" data-target="#email" data-toggle="modal">
							<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/envelope.png" />
							<span class="">E-mail</span>
						</a>
					</li>
					<li>
						<a href="" class="aam-call-item">
							<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/chat.png" />
							<span class="">Chat</span>
						</a>
					</li>
				</ul>
			</div>
		</section>
		<div class="container">
			<div class="clearfix copyright">
				<small>Copyright © 2017 - AAM Incorporadora. Todos os direitos reservados.</small>
				<div class="logo-rbianco">

				</div>
			</div>
		</div>
		<!-- / Chamada -->
	</footer>
	<?php wp_footer(); ?>
	<script type="text/javascript">
	$(window).on('load', function() {
	// $('#slider').nivoSlider({
	// 	controlNav: false
	// });
		$('#slider').bxSlider({
			pager: false
		});

		$(".owl-carousel").owlCarousel({
				autoplay:true,
				autoplayTimeout:6000,
				autoplayHoverPause:true,
				loop:true,
				margin:64,
				items: 5,
				nav:false,
				responsive : {
					380: {
						items:1
					},
						480 : { items : 1  }, // from zero to 480 screen width 4 items
						768 : { items : 2  }, // from 480 screen widthto 768 6 items
						1024 : { items : 4   // from 768 screen width to 1024 8 items
						},
							1600: { items: 4}
						// 1600: { items: 5 }
				}
		});

		$(window).on('scroll', function(){
			var scroll = $('.aam-footer').offset().top - ($('.aam-footer').offset().top * 0.25);

			if($(this).scrollTop() > 50 && scroll > $(this).scrollTop()){
					$('.aam-call').addClass('scroll');
			} else {
				$('.aam-call').removeClass('scroll');
			}

		});

		$('.search-icon').on('click', function(){
			if($('.search-box').hasClass('opened')){
				$('.search-box').removeClass('opened');
			} else {
				$('.search-box').addClass('opened');
			}
		});

	});


	$(function(){
		// $('.modal').modal('show');

		$('.circle').circleProgress().on('circle-animation-progress', function(event, progress, stepValue) {
			var percent = stepValue.toFixed(2).substr(2) == '00' ? '100' : stepValue.toFixed(2).substr(2);
			$(this).find('strong').html(percent + '<i>%</i>');

		});

		$('.planta').on('change', function(){
			var item = $(this).val();
			$('.aam-floor .slides').hide();
			$('.aam-floor h4.paragraph').hide();
			$('#' + item).removeClass('hide').fadeIn();
			$('#' + item + '-type').removeClass('hide').fadeIn();
		});

		$(document).ready(function(){
		  window.setTimeout(function(){
				$('a').nivoLightbox({
					effect: 'fade'
				});
			}, 100);
});


	});
	</script>
	</body>
</html>
