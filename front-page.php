<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package AAM_Incorporadora
 */

get_header(); ?>
<section class="aam-search">
<form class="form-inline">

	<select name="cidade" class="select">
		<option class="uppercase">Cidade:</option>
		<?php
			$field = get_field_object('cidade');
			foreach($field as $k => $v){
				if($k == 'value'){
					$val = explode(' - ',$v);
					echo '<option value="' . $val[1] . '">' . $val[0] . '</option>';
				}
			}
		?>
	</select>
	<select name="cidade" class="select">
		<option class="uppercase">Bairro:</option>
		<?php
			$field = get_field_object('bairro');
			foreach($field as $k => $v){
				if($k == 'value'){
					$val = explode(' - ',$v);
					echo '<option value="' . $val[1] . '">' . $val[0] . '</option>';
				}
			}
		?>
	</select>
	<select name="cidade" class="select">
		<option class="uppercase">Tipo:</option>
	</select>
	<select name="cidade" class="select">
		<option class="uppercase">Dormitórios:</option>
		<?php
			$field = get_field_object('dormitorios');
			foreach($field as $k => $v){
				if($k == 'value'){
					$val = explode(' - ',$v);
					echo '<option value="' . $val[1] . '">' . $val[0] . '</option>';
				}
			}
		?>
	</select>
	<select name="cidade" class="select">
		<option class="uppercase">Estágio da Obra:</option>
	</select>
	<button type="submit" class="button uppercase">Buscar</button>
</form>
</section>
<section class="aam-call">
<div class="container no-padding">
	<span class="broker">
		Fale agora mesmo com um
		<br />dos nossos consultores:
	</span>
	<ul>
		<li>
			<a href="" class="aam-call-item" data-target="#whatsapp" data-toggle="modal">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/whatsapp.png" />
				<span class="">WhatsApp</span>
			</a>
		</li>
		<li>
			<a href="" class="aam-call-item" data-target="#telefone" data-toggle="modal">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/telefone.png" />
				<span class="">Telefone</span>
			</a>
		</li>
		<li>
			<a href="" class="aam-call-item" data-target="#ligamos" data-toggle="modal">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/ligamos.png" />
				<span class="">Ligamos para você</span>
			</a>
		</li>
		<li>
			<a href="" class="aam-call-item" data-target="#email" data-toggle="modal">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/envelope.png" />
				<span class="">E-mail</span>
			</a>
		</li>
		<li>
			<a href="" class="aam-call-item">
				<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/chat.png" />
				<span class="">Chat</span>
			</a>
		</li>
	</ul>
</div>
</section>
<!-- / Chamada -->
<section class="aam-featured">
<div class="container">
	<h1 class="titulo">
		Conheça outros projetos
		<br /><span class="highlight">e realize seu sonho.</span>
	</h1>
	<div class="destaques">
		<?php
			$i = 1;
			while (have_rows('destaque', 784)) : the_row();
			$post_object = get_sub_field('empreendimento');

			if( $post_object ):
				$post = $post_object;
				setup_postdata( $post );
		?>

		<?php
			if($i == 1):
		?>
		<div class="destaque-maior">
			<div class="empreendimento hover_zoom" style="background: url('<?php the_field('bgparallax');?>');background-size: cover !important; ">
				<div class="empreendimento-descricao">
					<h2 class="titulo-empreendimento">
						<?php the_title() ?> <br />
						<span class="highlight"><?php the_field('bairro') ?></span>
					</h2>
					<ul class="informacao">
						<li>
							<span class="icone"></span> <?php the_field('metragem') ?>
						</li>
						<li>
							<span class="icone"></span> <?php the_field('dormitorios') ?>
						</li>
					</ul>
					<div class="empreendimento-chamada">
						<?php
							$parentcat = get_category_by_slug('imoveis');
							foreach((get_the_category()) as $childcat):
							if (cat_is_ancestor_of($parentcat, $childcat)):
						?>
							<span><?php echo $childcat->cat_name; ?></span>
						<?php
							endif;
							endforeach;
						?>

						<a href="<?php the_permalink() ?>" title="<?php the_title() ?>" target="_blank" class="button">Conhecer</a>
					</div>
				</div>
			</div>
		</div>
		<?php
				endif;
				$i++;
			endif;
		endwhile;
		?>


		<div class="destaques-menores">
			<?php
				$i = 1;
				while (have_rows('destaque', 784)) : the_row();
				$post_object = get_sub_field('empreendimento');

				if( $post_object ):
					$post = $post_object;
					setup_postdata( $post );
			?>
			<?php if($i == 2): ?>
				<div class="destaque-menor">
					<div class="empreendimento hover_zoom segundo-empreendimento" style="background: url('<?php the_field('bgparallax');?>'); background-size: cover !important;">
						<div class="empreendimento-descricao">
							<h2 class="titulo-empreendimento">
								<?php the_title() ?> <br />
								<span class="highlight"><?php the_field('bairro') ?></span>
							</h2>
							<ul class="informacao">
								<li>
									<span class="icone"></span> <?php the_field('metragem') ?>
								</li>
								<li>
									<span class="icone"></span> <?php the_field('dormitorios') ?>
								</li>
							</ul>
							<div class="empreendimento-chamada">
								<?php
									$parentcat = get_category_by_slug('imoveis');
									foreach((get_the_category()) as $childcat):
									if (cat_is_ancestor_of($parentcat, $childcat)):
								?>
									<span><?php echo $childcat->cat_name; ?></span>
								<?php
									endif;
									endforeach;
								?>

								<a href="<?php the_permalink() ?>" title="<?php the_title() ?>" target="_blank" class="button">Conhecer</a>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<?php if($i == 3): ?>
				<div class="destaque-menor">
					<div class="empreendimento hover_zoom terceiro-empreendimento" style="background: url('<?php the_field('bgparallax');?>'); background-size: cover !important;">
						<div class="empreendimento-descricao">
							<h2 class="titulo-empreendimento">
								<?php the_title() ?> <br />
								<span class="highlight"><?php the_field('bairro') ?></span>
							</h2>
							<ul class="informacao">
								<li>
									<span class="icone"></span> <?php the_field('metragem') ?>
								</li>
								<li>
									<span class="icone"></span> <?php the_field('dormitorios') ?>
								</li>
							</ul>
							<div class="empreendimento-chamada">
								<?php
									$parentcat = get_category_by_slug('imoveis');
									foreach((get_the_category()) as $childcat):
									if (cat_is_ancestor_of($parentcat, $childcat)):
								?>
									<span><?php echo $childcat->cat_name; ?></span>
								<?php
									endif;
									endforeach;
								?>

								<a href="<?php the_permalink() ?>" title="<?php the_title() ?>" target="_blank" class="button">Conhecer</a>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<?php endif;$i++; endwhile; ?>
		</div>

	</div>
</div>
</section>
<!-- /Destaque -->

<section class="aam-carousel">
<div class="owl-carousel">
	<?php
		while (have_rows('carrosel', 784)) : the_row();
		$post_object = get_sub_field('empreendimento');
		if( $post_object ):
			$post = $post_object;
			setup_postdata( $post );
	?>
	<div class="carousel-item" style="background: url(<?php the_field('bgparallax') ?>); background-size: cover;">
		<div class="carousel-overlay"></div>
		<div class="carousel-inner empreendimento">
			<div class="empreendimento-descricao">
				<h2 class="titulo-empreendimento">
					<?php the_title() ?> <br />
					<span class="highlight"><?php the_field('bairro') ?></span>
				</h2>
				<ul class="informacao">
					<li>
						<span class="icone"></span> <?php the_field('metragem') ?>
					</li>
					<li>
						<span class="icone"></span> <?php the_field('dormitorios') ?>
					</li>
				</ul>
				<div class="empreendimento-chamada">
					<?php
						$parentcat = get_category_by_slug('imoveis');
						foreach((get_the_category()) as $childcat):
						if (cat_is_ancestor_of($parentcat, $childcat)):
					?>
						<span><?php echo $childcat->cat_name; ?></span>
					<?php
						endif;
						endforeach;
					?>

					<a href="<?php the_permalink() ?>" title="<?php the_title() ?>" target="_blank" class="button">Conhecer</a>
				</div>
			</div>
		</div>
	</div>
	<?php
		endif;
		endwhile;
	?>
</div>
</section>

<section class="aam-our-history">
	<div class="container">
		<div class="aam-our-history-inner">
			<h3 class="subtitle">Tradição e pioneirismo</h3>
			<h2 class="title">Uma história construída <br />há mais de 15 anos.</h2>
			<p class="paragraph">
				Acompanhando a evolução urbana,os empreendimentos
	<br />com a marca AAM caminham junto à transformação de
	<br />São Paulo, contribuindo para a modernização da
	<br />arquitetura da cidade.
			</p>
			<a href="#" class="cta">Conheça nossa história.</a>
		</div>
	</div>
</section>

<?php
get_footer();
