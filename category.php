<?php
get_header(); ?>

<article class="aam-primary category">
  <div class="category-subheading">
    <div class="container">
      <?php custom_breadcrumbs() ?>
      <section class="aam-search">
      <form class="form-inline">
      	<select name="cidade" class="select">
      		<option class="uppercase">Cidade:</option>
      	</select>
      	<select name="cidade" class="select">
      		<option class="uppercase">Bairro:</option>
      	</select>
      	<select name="cidade" class="select">
      		<option class="uppercase">Tipo:</option>
      	</select>
      </form>
      </section>
    </div>
  </div>

	<div class="container">
    <h2 class="title"><?php single_cat_title() ?></h2>

    <?php if(have_posts()): ?>
    <div class="aam-carousel no-padding">
       <?php while (have_posts()) : the_post(); ?>
      <div class="carousel-item"  id="post-<?php the_ID(); ?>" style="background: url(<?php echo get_template_directory_uri()?>/assets/img/carousel/banner.png); background-size: cover;">
    		<div class="carousel-overlay"></div>
    		<div class="carousel-inner empreendimento">
          <div class="empreendimento-descricao">
            <h2 class="titulo-empreendimento">
              <?php the_title() ?> <br />
              <span class="highlight"><?php the_field('bairro') ?></span>
            </h2>
            <ul class="informacao">
              <li>
                <span class="icone"></span> <?php the_field('metragem') ?>
              </li>
              <li>
                <span class="icone"></span> <?php the_field('dormitorios') ?>
              </li>
            </ul>
            <div class="empreendimento-chamada">
              <?php
                $parentcat = get_category_by_slug('imoveis');
                foreach((get_the_category()) as $childcat):
                if (cat_is_ancestor_of($parentcat, $childcat)):
              ?>
                <span><?php echo $childcat->cat_name; ?></span>
              <?php
                endif;
                endforeach;
              ?>

              <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" target="_blank" class="button">Conhecer</a>
            </div>
          </div>
    		</div>
    	</div>
      <?php endwhile; ?>
    </div>
    <?php else : ?>
      <h2 class="center">Not Found</h2>
     <p class="center"><?php _e("Sorry, but you are looking for something that isn't here."); ?></p>
      <?php endif; ?>
	</div>
</article>
<?php
the_content();
get_footer();
