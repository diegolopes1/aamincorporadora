<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AAM_Incorporadora
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<?php wp_head(); ?>
</head>


<body <?php body_class(); ?>>
	<header class="topo">
		<div class="logo">
			<div class="container">
				<a href="<?php echo get_home_url() ?>" title="Visite a Home Page da AAM Incorporadora">
					<img src="<?php echo get_template_directory_uri()?>/assets/img/logo.png" />
				</a>

				<div class="search-box">
					<div class="search-box-inner">
						<input type="search" class="search-input" placeholder="Olá, digite aqui a palavra-chave" name="q" value="">
						<i class="search-icon">
							<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/search.png" alt="">
						</i>
					</div>
				</div>
			</div>
		</div>
		<div class="navigation">
			<nav class="navigation-wrapper container">
				<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_class' => 'primary-menu' ) ); ?>
			</nav>
		</div>
		<?php if(is_home() || is_front_page()): ?>
		<div class="aam-slider">
			<div class="container no-padding theme-default">

				<div id="slider" class="nivoSlider">
					<?php
						while (have_rows('slider', 784)) : the_row();
						$post_object = get_sub_field('empreendimento');
						if( $post_object ):
						// override $post
						$post = $post_object;
						setup_postdata( $post );
					?>

					<style media="all">
						.slide-post-id-<?php echo $post->ID ?> .aam-slider .caption .info, .slide-post-id-<?php echo $post->ID ?> .aam-slider .caption .caption-logo {
							border-color: <?php the_field('cor') ?>;
							color: <?php the_field('cor') ?>
						}

						/*.slide-post-id-<?php echo $post->ID ?> .button-2 {
							background-color: <?php the_field('cor') ?>;
						}*/
					</style>
						<div class="slide slide-post-id-<?php echo $post->ID ?>">
							<div class="overlay"></div>
							<img src="<?php the_field('bgparallax') ?>" alt="">
							<?php
								$exibicao = get_sub_field('imagem-exibicao');
							?>
							<div class="caption">
								<div class="caption-inner" style="background-color: <?php the_field('cor') ?>">
									<div class="caption-image hover_zoom" style="background: url('<?php echo $exibicao['sizes']['thumbnail-slider-featured'] ?>') no-repeat; background-size: cover;"></div>
									<div class="caption-content">
										<div class="caption-logo" style="background: url(<?php the_field('logo') ?>) no-repeat;">

										</div>
										<h3 class="caption-title">
											<?php the_field('chamada') ?>
										</h3>
										<span class="city"><?php the_field('cidade') ?></span>
										<ul class="info">
											<li><?php the_field('dormitorios') ?></li>
											<li><?php the_field('terreno') ?></li>
										</ul>
										<a href="<?php the_permalink() ?>" class="button-2 uppercase cta clearfix ">Quero conhecer</a>
									</div>
								</div>
							</div>
						</div>
					<?php
							endif;
						endwhile;
					?>
				</div>
			</div>
		</div>
		<!-- /slider -->
		<div class="navigation navigation-inner">
			<ul class="primary-menu">
				<li>
					<a href="">
						<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/predio.png" />
						<span class="">Lançamentos</span>
					</a>
				</li>
				<li>
					<a href="">
						<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/breve-lancamento.png" />
						<span class="">Breves Lançamentos</span>
					</a>
				</li>
				<li>
					<a href="">
						<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/em-obra.png" />
						<span class="">Em Obras</span>
					</a>
				</li>
				<li>
					<a href="">
						<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/prontos.png" />
						<span class="">Prontos</span>
					</a>
				</li>
			</ul>
		</div>
		<?php endif; ?>
	</header>

<body <?php body_class(); ?>>
