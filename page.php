<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package AAM_Incorporadora
 */

get_header(); ?>

<article class="aam-primary">
	<div class="container">
		<?php custom_breadcrumbs() ?>
		<!-- <ul class="breadcrumb">
			<li>Página Inicial</li>
			<li>Quem Somos</li>
		</ul> -->

		<!-- <h2 class="title">Um nome que se constrói a cada dia.</h2>
		<p class="paragraph">
			A história da AAM Incorporadora vem
			<br />acompanhando a evolução do mercador brasileiro
			<br />da construção civil. Diante da perspectiva de uma
			<br />forte demanda, passou a investir de forma
			<br />significativa na elaboração de projetos diferenciados
			<br />e de alto padrão.
		</p> -->

	</div>
</article>
<?php
the_content();
get_footer();
