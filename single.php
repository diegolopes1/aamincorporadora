<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package AAM_Incorporadora
 */

get_header(); ?>

<!-- -->

<style media="all">
.aam-post .post-information .localization ul li,
.aam-post .post-information .logo,
.aam-localization .mapa .tip,
.aam-floor .planta,
.aam-call-us .block-02 ul li img  {
	background: <?php the_field('cor') ?>;
}

.aam-call-us .block-02 ul li img   {
	opacity: 0.9;
}

.aam-post .title,
.aam-post .aam-call-us .wpcf7-form .wpcf7-submit {
	color: <?php the_field('cor') ?>;
}

.aam-post .subtitle:before {
	content: "";
	height: 1px;
	width: 60px;
	border: 1px solid <?php the_field('cor') ?>;
	position: absolute;
	top: 35px;
}

.aam-datasheet .datasheet-list li:before {
  content: "■";
  color: <?php the_field('cor') ?>;
  margin-left: -15px;
  margin-right: 5px;
}

.alpha60 {
		<?php
		$hex = get_field('cor');
			list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
		?>
    background-color: rgba(<?php echo $r ?>, <?php echo $g ?>, <?php echo $b ?>, 0.6);
}

.alpha40 {
		<?php
		$hex = get_field('cor');
			list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
		?>
    background-color: rgba(<?php echo $r ?>, <?php echo $g ?>, <?php echo $b ?>, 0.4);
}

.alpha20 {
		<?php
		$hex = get_field('cor');
			list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
		?>
    background-color: rgba(<?php echo $r ?>, <?php echo $g ?>, <?php echo $b ?>, 0.2);
}
</style>
	<article id="post-<?php the_ID(); ?>" class="aam-primary aam-post">
		<header class="post-header">
			<!-- <nav class="post-navigation alpha60" style="background-color: <?php the_field('cor') ?>"> -->
			<nav class="post-navigation">
				<div class="post-navigation-inner alpha40">
					<ul class="container">
						<li><a href="#projeto">Projeto</a></li>
						<?php if(have_rows('videos')): ?>
							<li><a href="#video">Vídeo</a></li>
						<?php endif; ?>
						<?php if(have_rows('fotos')): ?>
							<li><a href="#imagens">Imagens</a></li>
						<?php endif; ?>
						<?php if(have_rows('plantas')): ?>
							<li><a href="#plantas">Plantas</a></li>
						<?php endif; ?>
						<li><a href="#ficha">Ficha Técnica</a></li>
						<?php if(have_rows('tour-virtual')): ?>
							<li><a href="#tour">Tour Virtual</a></li>
						<?php endif; ?>
						<li><a href="#localizacao">Localização</a></li>
						<li><a href="#estagio">Estágio da Obra</a></li>
						<li><a href="#contato">Contato</a></li>
					</ul>
				</div>
			</nav>
			<div class="post-information" style="background-color: <?php the_field('cor') ?>">
				<div class="container">
					<div class="logo" style="background: <?php the_field('cor') ?>">
						<img src="<?php the_field('logo') ?>" alt="">
					</div>
					<div class="localization">
						<h3 class="name"><?php the_title() ?></h3>
						<h4 class="city"><?php the_field('bairro') ?></h4>
						<ul>
							<?php
								$parentcat = get_category_by_slug('imoveis');
								foreach((get_the_category()) as $childcat):
								if (cat_is_ancestor_of($parentcat, $childcat)):
							?>
							<li>
								<div class="alpha40">
									<?php echo $childcat->cat_name; ?>
								</div>
							</li>
							<?php

							endif;
							endforeach;
							?>
							<li>
								<div class="alpha60">
									<?php the_field('produto') ?>
								</div>
							</li>
						</ul>
					</div>
					<div class="slogan valign">
						<h3><?php the_field('chamada') ?></h3>
					</div>
				</div>
			</div>
		</header>

		<section id="projeto" class="aam-apresentation">
			<div class="container">
				<div class=""  style="background:url(<?php the_field('bgimg'); ?>) no-repeat #000; background-position:center center; background-size: cover;min-height: 850px;"></div>
			</div>
		</section>
		<!-- Imagem de Fundo -->

		<section class="aam-slogan">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<h3 class="subtitle">Projeto</h3>
						<h2 class="title" style="color: <?php the_field('cor') ?>"><?php the_field('chamada_2') ?></h2>
						<h4 class="paragraph"><?php the_field('chamada_3') ?></h4>
					</div>
					<div class="col-lg-6" style="min-height: 330px;">
						<p class="paragraph valign">
							<?php the_field('apresentacao') ?>
						</p>
					</div>
				</div>
			</div>
		</section>
		<!-- Slogan -->

		<?php if(have_rows('videos')): ?>
		<section id="video" class="aam-video">
			<div class="container">
				<h3 class="subtitle">Assista ao vídeo</h3>

				<iframe width="100%" height="450" src="<?php the_field('videos') ?>" frameborder="0" allowfullscreen></iframe>
			</div>
		</section>
		<!-- Vídeo -->
		<?php endif; ?>

		<?php if(have_rows('fotos')): ?>
		<section id="imagens" class="aam-gallery">
			<div class="container">
				<h3 class="subtitle">Galeria de Imagens</h3>
				<div class="tip">
					Clique na imagem para ampliá-la. <img src="<?php echo get_template_directory_uri()?>/assets/img/icons/lupa.png" alt="">
				</div>

				<div class="aam-gallery-inner">
					<?php
					while ( have_rows('fotos') ) : the_row();
						$imagem = get_sub_field('imagem');
					?>
					<div class="slides">
						<a href="<?php echo $imagem['url']; ?>"  data-lightbox-gallery="fotos-2" data-title="<?php the_sub_field('legenda'); ?>" title="<?php the_sub_field('legenda'); ?>">
							<div class="overlay" style="background: <?php the_field('cor') ?>"></div>
							<img src="<?php echo $imagem['sizes']['thumbnail-produto']; ?>" alt="<?php the_sub_field('legenda'); ?>" width="<?php echo $imagem['sizes']["thumbnail-produto-width"] ?>" height="<?php echo $imagem['sizes']["thumbnail-produto-height"] ?>">
							<span class="sub"><?php the_sub_field('legenda'); ?></span>
						</a>
					</div>
					<?php endwhile; ?>
				</div>
			</div>
		</section>
		<?php endif; ?>
		<!-- Galeria -->

		<?php if(have_rows('frase_de_impacto')): ?>
		<section class="aam-phrase" style="background: <?php the_field('cor_fundo_frase_de_impacto') ?>">
			<div class="container">
				<h2 class="title" style="color: <?php the_field('cor_texto_frase_de_impacto_copiar') ?>"><?php the_field('frase_de_impacto') ?></h2>
			</div>
		</section>
		<!-- Frase de Impacto -->
		<?php endif; ?>

		<?php if(have_rows('plantas')): ?>
		<section class="aam-floor">
			<div class="container">
				<h3 class="subtitle">Plantas</h3>
				<div class="row">

					<div class="col-lg-4 col-sm-6">
						<p>
							Espaço planejado para você
							<br />Selecione abaixo a planta desejada:

							<select class="planta" name="planta">
								<?php
									while ( have_rows('plantas') ) : the_row();
								?>
									<option value="planta-<?php echo the_sub_field('imagem') ?>"><?php the_sub_field('metragem'); ?></option>
								<?php
								endwhile;
								?>
							</select>

							<?php $x = 0; while ( have_rows('plantas') ) : the_row(); ?>
								<h4 class="paragraph <?php echo $x > 0 ? 'hide' : '' ?>" id="planta-<?php echo the_sub_field('imagem') ?>-type"><?php the_sub_field('legenda') ?></h4>
							<?php $x++; endwhile; ?>
						</p>
					</div>
					<div class="col-lg-8 col-sm-6">
						<?php
						$i = 0;
						while ( have_rows('plantas') ) : the_row();
							$imagem = get_sub_field('imagem');
							$attachment_id = $imagem;
							$imagemGaleria = "imgGaleria";
							$imagemFull = "full";
							$planta = wp_get_attachment_image_src( $attachment_id);

							$full = wp_get_attachment_image_src( $attachment_id, $imagemFull );
						?>

						<div id="planta-<?php echo $imagem ?>" class="slides <?php echo $i > 0 ? 'hide' : '' ?>">
							<a href="<?php echo $full[0]; ?>"  data-lightbox-gallery="photos" data-title="<?php the_sub_field('legenda'); ?>" title="<?php the_sub_field('legenda'); ?>">
								<div class="overlay" style="background: <?php the_field('cor') ?>"></div>
								<img src="<?php echo $full[0]; ?>" alt="<?php the_sub_field('legenda'); ?>" width="<?php echo $full[1]; ?>" height="<?php echo $full[2]; ?>">
							</a>
						</div>
						<?php $i++; endwhile; ?>
					</div>
				</div>
			</div>
		</section>
		<!-- Plantas -->
		<?php endif; ?>

		<section id="ficha" class="aam-datasheet">
			<div class="container">
				<h3 class="subtitle">Ficha Técnica</h3>
				<?php
					$fields = array(
						0 => array(
							'Dormitórios',
							'dormitorios'
						),
						1 => array(
							'Metragem',
							'metragem'
						),
						2 => array(
							'Lazer / Área comum',
							'lazer'
						),
						3 => array(
							'Área do Terreno',
							'terreno'
						),
						4 => array(
							'Nº de Vagas por Unidade',
							'vagas'
						),
						5 => array(
							'Nº Pavimentos',
							'pavimentos'
						),
						6 => array(
							'Nº de torres',
							'torres'
						),
						7 => array(
							'Nº Total de Unidades',
							'unidades-total'
						),
						8 => array(
							'Nº de Unidades por Andar',
							'unidades-andar'
						),
						9 => array(
							'Realização',
							'realizacao'
						),
						10 => array(
							'Construção',
							'construcao'
						),
						11 => array(
							'Projeto Arquitetônico',
							'arquitetura'
						),
						12 => array(
							'Projeto de Paisagismo',
							'paisagismo'
						),
						13 => array(
							'Projeto de Interiores',
							'decoracao'
						),
						14 => array(
							'Vendas',
							'vendas'
						),
						15 => array(
							'Nº Elevadores',
							'elevadores'
						),
						16 => array(
							'Diferenciais Terreno',
							'diferenciais_terreno'
						),
						17 => array(
							'Diferenciais Apartamento',
							'diferenciais_apartamento'
						),
						18 => array(
							'Diferenciais Prédio',
							'diferenciais_predio'
						),
						19 => array(
							'Diferenciais Garagem',
							'diferenciais_garagem'
						),
						20 => array(
							'Campo Extra 1',
							'campo-extra-1'
						),
						21 => array(
							'Campo Extra 2',
							'campo-extra-2'
						),
						22 => array(
							'Campo Extra 3',
							'campo-extra-3'
						),
						23 => array(
							'Campo Extra 4',
							'campo-extra-4'
						),
					);
				?>
				<ul class="datasheet-list">
					<?php
						foreach($fields as $field):
							if(get_field($field[1])):
					?>
						<li>
								<?php echo $field[0] ?>:
								<span style="color: <?php the_field('cor') ?>"><?php the_field($field[1]) ?></span>
						</li>
					<?php
							endif;
						endforeach;
					?>
				</ul>
			</div>
		</section>
		<!-- Ficha Técnica -->

		<?php if(have_rows('tour-virtual')): ?>
		<section id="tour" class="aam-tour-virtual">
			<div class="container">
				<h3 class="subtitle">Tour Virtual</h3>
				<iframe src="" border="0" width="100%" height="440"></iframe>
			</div>
		</section>
		<!-- Tour Virtual -->
		<?php endif; ?>

		<?php if( get_field('endereco') != ''): ?>
		<section id="localizacao" class="aam-localization">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<h3 class="subtitle">Localização</h3>
						<addreess>
							<h1 class="title"><?php the_field('endereco') ?></h1>
							<span style="color: <?php the_field('cor') ?>"><?php the_field('bairro') ?>, <?php the_field('cidade') ?></span>
						</addreess>
					</div>
				</div>

				<div class="row" style="margin-top: 35px;">
						<?php $full = ''; if(get_field('chamada_complementar') || get_field('endereco_slogan') || get_field('endereco_descricao')): $full = false ?>
						<div class="col-lg-6 col-xs-12">
							<address>
								<div class="chamada-complementar">
									<?php the_field('chamada_complementar') ?>
								</div>
								<div class="chamada-slogan">
									<?php the_field('endereco_slogan') ?>
								</div>
								<div class="chamada-descricao">
									<?php the_field('endereco_descricao') ?>
								</div>
							</addreess>
						</div>
						<?php endif; ?>
						<div class="<?php echo $full == true ? 'col-lg-12' : 'col-lg-6' ?> col-xs-12 relative mapa">
							<div class="tip">
								<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/lupa-branca.png" alt=""> Ampliar Mapa
							</div>
							<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAVczTZXiNOrF_8fitX-1da4CTzK8SwaYY&q=<?php the_field('endereco').'-'.the_field('bairro').'-'.the_field('cidade') ?>,84020&zoom=14&maptype=roadmap" width="100%" height="550" border="0"></iframe>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php endif; ?>
		<!-- Localização -->

		<?php if(have_rows('fotos_rua')): ?>
		<section class="aam-gallery">
			<div class="container">
				<div class="aam-gallery-inner owl-carousel">
					<?php
					while ( have_rows('fotos_rua') ) : the_row();
						$imagem = get_sub_field('imagem');
					?>
					<div class="slides">
						<a href="<?php echo $imagem['url']; ?>"  data-lightbox-gallery="roadtrip" data-title="<?php the_sub_field('legenda'); ?>" title="<?php the_sub_field('legenda'); ?>">
							<div class="overlay" style="background: <?php the_field('cor') ?>"></div>
							<img src="<?php echo $imagem['sizes']['thumbnail-produto']; ?>" alt="<?php the_sub_field('legenda'); ?>" width="<?php echo $imagem['sizes']["thumbnail-produto-width"] ?>" height="<?php echo $imagem['sizes']["thumbnail-produto-height"] ?>">
							<span class="sub"><?php the_sub_field('legenda'); ?></span>
						</a>
					</div>
					<?php endwhile; ?>
				</div>
			</div>
		</section>
		<?php endif; ?>

		<section id="estagio" class="aam-stages">
			<div class="container">
				<h3 class="subtitle">Estágio da Obra</h3>

				<div class="aam-circle">
					<?php
						$data = array(
							0 => array(
								'Total da Obra',
								'total'
							),
							1 => array(
								'Lançamento',
								'lancamento'
							),
							2 => array(
								'Projetos',
								'projetos'
							),
							3 => array(
								'Contenções',
								'contencoes'
							),
							4 => array(
								'Terraplanagem',
								'terraplanagem'
							),
							5 => array(
								'Tirantes',
								'tirantes'
							),
							6 => array(
								'Estrutura',
								'estrutura'
							),
							7 => array(
								'Alvenaria',
								'alvenaria'
							),
							8 => array(
								'Revestimentos Internos',
								'revestimentos'
							),
							9 => array(
								'Esquadrias',
								'esquadrias'
							),
							10 => array(
								'Fachadas',
								'fachadas'
							),
							11 => array(
								'Pisos',
								'pisos'
							),
							12 => array(
								'Instalações Elétricas',
								'instalacoes'
							),
							13 => array(
								'Instalações Hidráulicas',
								'instalacoes_2'
							),
							14 => array(
								'Louças e Metais',
								'loucas'
							),
							15 => array(
								'Pinturas',
								'pinturas'
							),
							16 => array(
								'Paisagismo',
								'paisagismo'
							),
							17 => array(
								'Fundação',
								'fundacao'
							)
						);

						foreach($data as $field):
							if(get_field($field[1])):
								$percent = get_field($field[1])/100;
								$color = ($percent > 0.15) ? get_field('cor') : '#811114';

					?>
					<div class="circle" data-value="<?php echo $percent ?>" data-thickness="15" data-animation-start-value="3.0" data-fill="{&quot;color&quot;: &quot;<?php echo $color ?>&quot;}">
						<strong>0<i>%</i></strong>
						<h4><?php echo $field[0] ?></h4>
					</div>
				<?php
						endif;
					endforeach;
				?>
				</div>
			</div>
		</section>
		<!-- Estágios da Obra -->

		<?php if(have_rows('fotos-obra')): ?>
		<section class="aam-gallery">
			<div class="container">
				<div class="aam-gallery-inner owl-carousel">
					<?php
					while ( have_rows('fotos-obra') ) : the_row();
						$imagem_id = get_sub_field('imagem');
						$imagem = wp_get_attachment_image_src( $imagem_id, 'thumbnail-produto');

						$full = wp_get_attachment_image_src( $imagem_id, 'full' );
					?>
					<div class="slides">
						<a href="<?php echo $full[0]; ?>"  data-lightbox-gallery="roadtrip2" data-title="<?php the_sub_field('legenda'); ?>" title="<?php the_sub_field('legenda'); ?>">
							<div class="overlay" style="background: <?php the_field('cor') ?>"></div>
							<img src="<?php echo $imagem[0]; ?>" alt="<?php the_sub_field('legenda'); ?>" width="$imagem[1]" height="$imagem[2]">
							<span class="sub"><?php the_sub_field('legenda'); ?></span>
						</a>
					</div>
					<?php endwhile; ?>
				</div>
			</div>
		</section>
		<?php endif; ?>
		<!-- Fotos da Obra -->

		<section class="aam-parallax">
			<div style="background:url(<?php the_field('bgparallax'); ?>) no-repeat #000; background-size:cover; background-position:center center; background-attachment:fixed; min-height: 410px;">

			</div>
		</section>
		<!-- Parallax -->

		<section id="contato" class="aam-call-us" style="background-color: <?php the_field('cor') ?>">
			<div class="container">
				<div class="block-01">
					<h3 class="subtitle">Fale Conosco</h3>
					<h1 class="title">
						Fale agora
						<br />mesmo com
						<br />um dos nossos
						<br />consultores.
					</h1>
					<p>Selecione a opção desejada.</p>
				</div>

				<div class="block-02">
					<div class="">
						<ul>
							<li data-toggle="modal" data-target="">
								<div class="alpha60">
									<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/chat-branco.png" alt="">
									Consultor
									<br />Online
								</div>
							</li>
							<li data-toggle="modal" data-target="#telefone">
								<div class="alpha40">
									<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/telefone-branco.png" alt="">
									Telefone
									<br />para contato
								</div>
							</li>
							<li class="ligamos" data-toggle="modal" data-target="#ligamos">
								<div class="alpha20">
									<img src="<?php echo get_template_directory_uri()?>/assets/img/icons/ligamos-para-voce-branco.png" alt="">
									Ligamos
									<br />para você
								</div>
							</li>
						</ul>
					</div>
				</div>

				<div class="block-03">
					<?php echo do_shortcode('[contact-form-7 id="774" title="Fale Conosco - Interna Produto"]') ?>
				</div>
			</div>
		</section>
	</article>
<?php
get_footer();
